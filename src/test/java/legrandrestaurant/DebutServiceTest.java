package legrandrestaurant;

import org.junit.Test;
import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class DebutServiceTest {

	public static final int nombres_Table = 8; 

	@Test
	public void tablesAssigneAUnServeur()
    {
		//ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur
		Restaurant restaurant = new Restaurant(1, nombres_Table);
		restaurant.getTables_Libres().get(0).setAssigneServeur(restaurant.getServeur().get(0));

		// QUAND le service commence
		restaurant.service_Start();

		// ALORS cette table est toujours affectée au serveur et les deux autres au maître d'hôtel
		ArrayList<Table> tablesRestaurant = restaurant.getTables_Libres();

		// vérifie si la première table est assigné au serveur
		boolean result = (tablesRestaurant.get(0).getAssigneServeur() == restaurant.getServeur().get(0));

		// vérifie si le reste des tables sont assignées au maître d'hôtel
		for(int i=1; i<tablesRestaurant.size(); i++){
			//result = result && (tablesRestaurant.get(i).getAssigneServeur() == restaurant.getMaitrehotel());
		}
		assertTrue(result);
    }

	@Test
	public void assigneServeurApresServiceCommence()
    {
		// ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un serveur
		Restaurant restaurant = new Restaurant(2, nombres_Table);
		restaurant.getTables_Libres().get(0).setAssigneServeur(restaurant.getServeur().get(0));

		// QUAND le service commence
		restaurant.service_Start();

		// ALORS il n'est pas possible de modifier le serveur affecté aux tables
		// setAssigneA(...) return false si il n'a pas réussi modifier le serveur affecter à la table
		boolean result = restaurant.getTables_Libres().get(0).setAssigneServeur(restaurant.getServeur().get(1));
		assertFalse(result);
    }

	@Test
	public void chgtAffectationFinService()
    {
		// ÉTANT DONNÉ un restaurant ayant 3 tables dont une affectée à un premier serveur
		Restaurant restaurant = new Restaurant(2, nombres_Table);
		Serveur serveur1 = restaurant.getServeur().get(0);
		restaurant.getTables_Libres().get(0).setAssigneServeur(serveur1);

		// ET ayant débuté son service
		restaurant.service_Start();
		
		// QUAND le service se termine
		restaurant.service_End();

		// ET que cette table est affectée à un second serveur
		Serveur serveur2 = restaurant.getServeur().get(1);
		restaurant.getTables_Libres().get(0).setAssigneServeur(serveur2);
		
		// ALORS la table éditée est affectée au serveur et les deux autres au maître d'hôtel
		boolean result = (restaurant.getTables_Libres().get(0).getAssigneServeur() == serveur2);
		assertTrue(result);
    }
}