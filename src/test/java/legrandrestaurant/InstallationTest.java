package legrandrestaurant;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class InstallationTest {

    public static final int nbrServeurs = 0;
    
    public static final int nbrTable = 1; 

    @Test
    public void tableOccupee()
    {
        // ÉTANT DONNE une table dans un restaurant ayant débuté son service
        Restaurant restaurant = new Restaurant(nbrServeurs, nbrTable);
        Table table = restaurant.getTables_Libres().get(nbrTable-1);

        // QUAND un client est affecté à une table
        restaurant.new_table_Occupee(table);

        // ALORS cette table n'est plus sur la liste des tables libres du restaurant
        boolean result = (restaurant.getTables_Libres().size() == (nbrTable-1));
        assertTrue(result);
    }

    @Test
    public void tableLibre()
    {
        // ÉTANT DONNE une table occupée par un client
        Restaurant restaurant = new Restaurant(0, nbrTable);
        Table table = restaurant.getTables_Libres().get(nbrTable-1);
        restaurant.new_table_Occupee(table);

        // QUAND la table est libérée
        restaurant.new_table_Libre(table);
        
        // ALORS cette table apparaît sur la liste des tables libres du restaurant
        boolean result = (restaurant.getTables_Libres().get(nbrTable-1) == table);
        assertTrue(result);
    }
}
