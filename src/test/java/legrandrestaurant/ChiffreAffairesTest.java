package legrandrestaurant;

import org.junit.Test;
// import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class ChiffreAffairesTest
{ // ca = chiffre d'affaires
// SCOPE ÉTANT DONNÉ un nouveau serveur
//QUAND il prend une commande
//ALORS son chiffre d'affaires est le montant de celle-ci
    @Test
    public void caNouveauServeur()
    {
        // Étant donné un nouveau serveur
        Serveur serveur = new Serveur();
        // Quand on récupère son chiffre d'affaires
        double caServeur = serveur.getChiffreAffaires();
        // Alors celui-ci est à 0
        assertEquals(0, caServeur, 0);
    }

    //		ÉTANT DONNÉ un serveur ayant déjà pris une commande
    //		QUAND il prend une nouvelle commande
    //	ALORS son chiffre d'affaires est la somme des deux commandes
    @Test
    public void caServeurCommande()
    {
        // ÉTANT DONNÉ un nouveau serveur
        Serveur serveur = new Serveur();

	// QUAND il prend une commande
        double totalCommande = 34;
        serveur.setCommande(totalCommande);
 
		// ALORS son chiffre d'affaires est le montant de celle-ci
        double caServeur1 = serveur1.getChiffreAffaires();
        assertEquals(totalCommande, caServeur1, 0);
    }

    @Test
    public void caServeurCommandes2()
    {
        // ÉTANT DONNÉ un serveur ayant déjà pris une commande
        Serveur serveur = new Serveur();
        double totalCommande1 = 34;
        serveur.setCommande(totalCommande1);

		// QUAND il prend une nouvelle commande
        double totalCommande2 = 22;
        serveur.setCommande(totalCommande2);

		// ALORS son chiffre d'affaires est la somme des deux commandes
        double caServeur = serveur.getChiffreAffaires();
        assertEquals((totalCommande1+totalCommande2), caServeur, 0);
    }


// SCOPE Restaurant
    //ÉTANT DONNÉ un restaurant ayant X serveurs
	//	QUAND tous les serveurs prennent une commande d'un montant Y
	//	ALORS le chiffre d'affaires du restaurant est X * Y
	//	CAS(X = 0; X = 1; X = 2; X = 100)
	//	CAS(Y = 1.0)
    
    
// SCOPE Franchise
    //ÉTANT DONNÉ une franchise ayant X restaurants de Y serveurs chacuns
	//	QUAND tous les serveurs prennent une commande d'un montant Z
	//	ALORS le chiffre d'affaires de la franchise est X * Y * Z
	//	CAS(X = 0; X = 1; X = 2; X = 1000)
	//	CAS(Y = 0; Y = 1; Y = 2; Y = 1000)
	//	CAS(Z = 1.0)
    @Test
    public void caFranchise(int x, int y, double z)
    {
        // ÉTANT DONNÉ une franchise ayant X restaurants de Y serveurs chacuns
        ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
        for(int i=0; i<x ; i++)
        {
            Restaurant restaurant = new Restaurant(y, 0);
            restaurants.add(restaurant);
        }

		// QUAND tous les serveurs prennent une commande d'un montant Z
        for(int i=0; i<restaurants.size() ; i++)
        {
            ArrayList<Serveur> serveursRestaurant = restaurants.get(i).getServeur();
            for(int j=0; j<restaurants.size() ; j++)
                serveursRestaurant.get(j).setCommande(z);
        }

		// ALORS le chiffre d'affaires de la franchise est X * Y * Z
        double caFranchise = 0;
        for(int i=0; i<restaurants.size() ; i++)
        {
            ArrayList<Serveur> serveursRestaurant = restaurants.get(i).getServeur();
            for(int j=0; j<serveursRestaurant.size() ; j++)
                caFranchise += serveursRestaurant.get(i).getChiffreAffaires();
        }
        assertEquals((x*y*z), caFranchise, 0);
    }
}