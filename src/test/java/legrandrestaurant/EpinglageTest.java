package legrandrestaurant;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EpinglageTest {
    
    @Test
    public void commandeNonPayee(){
        // ÉTANT DONNE un serveur ayant pris une commande
        Serveur serveur = new Serveur();
        Commande commande = new Commande("Salade");
        serveur.setCommande(commande);

        // QUAND il la déclare comme non-payée
        serveur.commandeNonPayee(serveur.getCommande(0));

        // ALORS cette commande est marquée comme épinglée
        assertEquals(commande, serveur.getCommande(0));
    }

    @Test
    public void transmisGendarmerie(){
        // ÉTANT DONNE un serveur ayant épinglé une commande
        Restaurant restaurant = new Restaurant(1, 0);
        Serveur serveur = restaurant.getServeur().get(0);
        serveur.setCommande(new Commande("Poisson"));
        serveur.commandeNonPayee(serveur.getCommande(0));

        // QUAND elle date d'il y a au moins 15 jours
        Commande commandeEnregistre = serveur.getCommande(0);
        commandeEnregistre.setNbrJourEnregistre(3);
        serveur.checkNbrJourEnregistre(commandeEnregistre);

        // ALORS cette commande est marquée comme à transmettre à la gendarmerie
        assertEquals(commandeEnregistre, restaurant.getCommandeGendarmerie(0));
    }

    @Test
    public void listeTransmettreGendarmerie(){
        // ÉTANT DONNE une commande à transmettre gendarmerie
        Restaurant restaurant = new Restaurant(1, 0);
        Serveur serveur = restaurant.getServeur().get(0);
        serveur.setCommande(new Commande("Viande"));
        serveur.commandeNonPayee(serveur.getCommande(0));
        Commande commandeEnregistre = serveur.getCommande(0);
        commandeEnregistre.setNbrJourEnregistre(2);
        serveur.checkNbrJourEnregistre(commandeEnregistre);

        // QUAND on consulte la liste des commandes à transmettre du restaurant
        Commande commandeListeATransmettreDuRestaurant = restaurant.getCommandeGendarmerie(0);

        // ALORS elle y figure
        assertEquals(commandeEnregistre, commandeListeATransmettreDuRestaurant);
    }

    @Test
    public void commandeTransmise(){
        // ÉTANT DONNE une commande à transmettre gendarmerie
        Restaurant restaurant = new Restaurant(1, 0);
        Serveur serveur = restaurant.getServeur().get(0);
        serveur.setCommande(new Commande("Patate"));
        serveur.commandeNonPayee(serveur.getCommande(0));
        Commande commandeEnregistre = serveur.getCommande(0);
        commandeEnregistre.setNbrJourEnregistre(3);
        serveur.checkNbrJourEnregistre(commandeEnregistre);

        // QUAND elle est marquée comme transmise à la gendarmerie
        restaurant.transmissionCommandeGendarmerie(restaurant.getCommandeGendarmerie(0));

        // ALORS elle ne figure plus dans la liste des commandes à transmettre du restaurant
        assertEquals(0, restaurant.getNbrCommandesGendarmerie());
    }
}
