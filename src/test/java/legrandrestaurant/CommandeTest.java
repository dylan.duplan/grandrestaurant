package legrandrestaurant;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CommandeTest {

    public static final int nbrServeurs = 1; 
    
    public static final int nbrTable = 0;

    //	ÉTANT DONNE un serveur dans un restaurant
	//QUAND il prend une commande de nourriture
    //ALORS cette commande apparaît dans la liste de tâches de la cuisine de ce restaurant

    @Test
    public void commandeNourriture()
    {
        // ÉTANT DONNE un serveur dans un restaurant
        Restaurant restaurant = new Restaurant(nbrServeurs, nbrTable);
        Serveur serveur = restaurant.getServeur().get(nbrServeurs-1);

        // QUAND il prend une commande de nourriture
        Commande commande = new Commande("Spaghetti");
        serveur.setCommande(commande);

        // ALORS cette commande apparaît dans la liste de tâches de la cuisine de ce restaurant
        assertEquals(commande, restaurant.getCuisine().getCommandes().get(0));
    }

    //ÉTANT DONNE un serveur dans un restaurant
	//QUAND il prend une commande de boissons
	//ALORS cette commande n'apparaît pas dans la liste de tâches de la cuisine de ce restaurant
    @Test
    public void commandeBoisson()
    {
        // ÉTANT DONNE un serveur dans un restaurant
        Restaurant restaurant = new Restaurant(nbrServeurs, nbrTable);
        Serveur serveur = restaurant.getServeur().get(nbrServeurs-1);

        // QUAND il prend une commande de boissons
        serveur.setCommande("Eau");

        // ALORS cette commande n'apparaît pas dans la liste de tâches de la cuisine de ce restaurant
        assertEquals(0, restaurant.getCuisine().getCommandes().size());
    }
    
}
