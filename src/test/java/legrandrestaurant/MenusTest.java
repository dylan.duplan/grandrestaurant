package legrandrestaurant;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MenusTest {

    @Test
    public void majPrixPlat(){
        // ÉTANT DONNE un restaurant ayant le statut de filiale d'une franchise
        Franchise lerestau = new Franchise();
        Restaurant restaurant = new Restaurant(lerestau);

        // ET une franchise définissant un menu ayant un plat
        Plat plat = new Plat("poisson", 10);
        lerestau.addPlat(plat);

        // QUAND la franchise modifie le prix du plat
        double nouveauPrix = 8;
        lerestau.modifPrix(0, nouveauPrix);

        // ALORS le prix du plat dans le menu du restaurant est celui défini par la franchise
        assertEquals(nouveauPrix, restaurant.getMenu().get(0).getPrix(), 0);
    }

    @Test
    public void majPrixPlatFanchise(){
        // ÉTANT DONNE un restaurant appartenant à une franchise et définissant un menu ayant un plat
        Franchise lerestau = new Franchise();
        Restaurant restaurant = new Restaurant(lerestau);
        
        Plat plat = new Plat("poisson", 10);
        restaurant.addPlat(plat);

        // ET une franchise définissant un menu ayant le même plat
        Plat plat2 = new Plat("salade", 12);
        lerestau.addPlat(plat2);

        // QUAND la franchise modifie le prix du plat
        double nouveauPrix = 15;
        lerestau.modifPrix(0, nouveauPrix);

        // ALORS le prix du plat dans le menu du restaurant reste inchangé
        assertNotEquals(restaurant.getPlat(plat.getLibelle()).getPrix(), nouveauPrix);
    }

    @Test
    public void nvPlatFranchise(){
        // ÉTANT DONNE un restaurant appartenant à une franchise et définissant un menu ayant un plat
        Franchise lerestau = new Franchise();
        Restaurant restaurant = new Restaurant(lerestau);
        
        Plat plat1 = new Plat("frite", 5);
        restaurant.addPlat(plat1);

        // QUAND la franchise ajoute un nouveau plat
        Plat plat2 = new Plat("Tartare", 7.89);
        lerestau.addPlat(plat2);

        // ALORS la carte du restaurant propose le premier plat au prix du restaurant et le second au prix de la franchise
        assertEquals(plat1, restaurant.getMenu().get(0));
        assertEquals(plat2, restaurant.getMenu().get(1));
    }
}