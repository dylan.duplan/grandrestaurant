package legrandrestaurant;

public class Commande {

    private String nom;

    private int nbr_Jour_Enregistre;

    public Commande(String nom) {
        this.nom = nom;
    }

    public String getNom(){
        return this.nom;
    }

    public void setNbrJourEnregistre(int nbr_Jour_Enregistre) {
        this.nbr_Jour_Enregistre = nbr_Jour_Enregistre;
    }

    public int getNbrJourEnregistre() {
        return this.nbr_Jour_Enregistre;
    }
}
