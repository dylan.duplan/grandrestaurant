package legrandrestaurant;

public class Table {

    private Restaurant restaurant;

    private Serveur serveur;
    
    public Table(Restaurant restaurant){
        this.restaurant = restaurant;
    }    

    public Table(Restaurant restaurant, Serveur serveur){
        this.restaurant = restaurant;
        this.serveur = serveur;
    }

    public boolean setAssigneServeur(Serveur serveur) {
        if(this.restaurant.getServiceEnCours() ==  false){
            this.serveur = serveur;
            return true;
        }
        return false;
    }

    public Object getAssigneServeur() {
        if(this.serveur != null){
            return this.serveur;
        }
    }
}
