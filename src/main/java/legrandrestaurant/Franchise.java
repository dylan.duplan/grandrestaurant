package legrandrestaurant;

import java.util.ArrayList;

public class Franchise {

    private ArrayList<Plat> menu = new ArrayList<>();

    public void addPlat(Plat plat){
        menu.add(plat);
    }

    public ArrayList<Plat> getMenu() {
        return this.menu;
    }

    public void modifPrix(int index, double nouveau_Prix){
        this.menu.get(index).setPrix(nouveau_Prix);
    }
}
