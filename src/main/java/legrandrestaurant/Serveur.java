package legrandrestaurant;

import java.util.ArrayList;

public class Serveur {

    private Restaurant restaurant;

    private ArrayList<Commande> commandes = new ArrayList<>();
    
    private ArrayList<Commande> commandes_enregistre = new ArrayList<>();

    private double chiffre_Affaires;

    public Serveur(){}

    public Serveur(Restaurant restaurant){
        this.restaurant = restaurant;
    }

    public double getChiffreAffaires() {
        return chiffre_Affaires;
    }

    public void attribueCommande(double total) {
        chiffre_Affaires += total;
    }

    public void setCommande(Commande commande) {
        this.restaurant.getCuisine().addCommande(commande);
    }

    public Commande getCommande(int indexCommande) {
        return this.commandes.get(indexCommande);
    }

    public void commandeNonPayee(Commande commande) {
        commandes_enregistre.add(commande);
        commandes.remove(commande);
    }

    public Commande commandeEnregistre(int index) {
        return commandes_enregistre.get(index);
    }

    public void CommandeGendarmerie(Commande commandeEnregistre) {
        restaurant.addCommandeGendarmerie(commandeEnregistre);
        commandes_enregistre.remove(commandeEnregistre);
    }

    public void checkNbrJourEnregistre(Commande commandeEnregistre) {
        int nbrJourEpinglee = commandeEnregistre.getNbrJourEnregistre();
        if(nbrJourEpinglee >= 15) {
            CommandeGendarmerie(commandeEnregistre);
        }
    }
    
        public void attribueCommande(Commande commande) {
        this.commandes.add(commande);
    }

}
