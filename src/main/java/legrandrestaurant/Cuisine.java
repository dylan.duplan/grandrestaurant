package legrandrestaurant;

import java.util.ArrayList;

public class Cuisine {
    
    private ArrayList<Commande> liste_Commandes = new ArrayList<Commande>();

    public void addCommande(Commande commande) {
        this.liste_Commandes.add(commande);
    }

    public ArrayList<Commande> getCommandes() {
        return this.liste_Commandes;
    }

    // public Commande getListeCommandes(int index) {
    //     return this.listeCommandes.get(index);
    // }

}
