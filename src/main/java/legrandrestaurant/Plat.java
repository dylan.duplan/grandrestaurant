package legrandrestaurant;

public class Plat {

    private String libelle;

    private double prix;

    public Plat(String libelle, double prix){
        this.libelle = libelle;
        this.prix = prix;
    }

    public double getPrix(){
        return prix;
    }

    public void setPrix(double nouveauPrix) {
        this.prix = nouveauPrix;
    }

    public String getLibelle() {
        return this.libelle;
    }
}
