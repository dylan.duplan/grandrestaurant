package legrandrestaurant;

import java.util.ArrayList;

public class Restaurant {

    private Franchise franchise = null;

    private Cuisine cuisine = new Cuisine();

    private ArrayList<Serveur> serveurs = new ArrayList<>();

    private ArrayList<Table> tables_Libres = new ArrayList<>();

    private ArrayList<Table> tables_Ocuppees = new ArrayList<>();

    private boolean service_En_Cours = false;

    private ArrayList<Plat> menu = new ArrayList<>();

    private ArrayList<Commande> commande_Gendarmerie = new ArrayList<>();

    public Restaurant(Franchise franchise){
        this.franchise = franchise;
    }

    public Restaurant(int nombre_Serveurs, int nombre_Tables) {

        for(int i=0; i<nombre_Serveurs; i++) {
            this.serveurs.add(new Serveur(this));
        }

        for(int i=0; i<nombre_Tables; i++) {
            this.tables_Libres.add(new Table(this));
        }
    }

    public ArrayList<Serveur> getServeur() {
        return this.serveurs;
    }

    public void new_table_Libre(Table table_Libre){
        this.tables_Ocuppees.remove(table_Libre);
        this.tables_Libres.add(table_Libre);
    }

    public void new_table_Occupee(Table table_Occupee){
        this.tables_Libres.remove(table_Occupee);
        this.tables_Ocuppees.add(table_Occupee);
    }

    public ArrayList<Table> getTables_Libres() {
        return this.tables_Libres;
    }

    public ArrayList<Table> getTables_Occupees() {
        return this.tables_Ocuppees;
    }


    public boolean getServiceEnCours() {
        return this.service_En_Cours;
    }

    public void service_Start() {
        service_En_Cours = true;
    }

    public void service_End() {
        service_En_Cours = false;
    }

    public Cuisine getCuisine() {
        return this.cuisine;
    }
    
    public void addPlat(Plat nouveau_Plat){
        this.menu.add(nouveau_Plat);
    }
     

    public Plat getPlat(String nom_Plat){
        for(int i=0; i<menu.size(); i++){
            if(this.menu.get(i).getNom() == nom_Plat){
                return this.menu.get(i);
            }
        }
        ArrayList<Plat> menuFranchise = this.franchise.getMenu();
        for(int i=0; i<menuFranchise.size(); i++){
            if(menuFranchise.get(i).getNom() == nom_Plat){
                return menuFranchise.get(i);
            }
        }
        return null;
    }
    
    public ArrayList<Plat> getMenu() {
        ArrayList<Plat> menuComplet = this.menu;
        menuComplet.addAll(franchise.getMenu());
        return menuComplet;
    }

  

    public void addCommandeGendarmerie(Commande commande) {
        commande_Gendarmerie.add(commande);
    }

    public Commande getCommandeGendarmerie(int index) {
        return commande_Gendarmerie.get(index);
    }

    public int getNbrCommandesGendarmerie() {
        return commande_Gendarmerie.size();
    }

    public void transmissionCommandeGendarmerie(Commande commande){
        commande_Gendarmerie.remove(commande);
    }

}
